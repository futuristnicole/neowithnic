import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import TopNav from './components/menu/TopNav';
import Home from './components/Home';
import Tutorials from './components/Tutorials';
import Projects from './components/Projects';
import About from './components/About';
import Contact from './components/Contact';
import Footer from './components/menu/Footer';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Route path="/" component={TopNav} />
        <Switch>
          <Route path="/tutorials"  component={Tutorials} />
          <Route path="/projects"  component={Projects} />
          <Route path="/about"  component={About} />
          <Route path="/contact"  component={Contact} />
          <Route path="/"  component={Home} />
        </Switch>
        <Route path="/" component={Footer } />
      </BrowserRouter>
    </div>
  );
}

export default App;