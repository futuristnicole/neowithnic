import React, { Component } from 'react';
import KokoHomeHero from './kokohome/kokohomehero';
// import KokoHomeAbout from './kokohome/KokoHomeAbout';
// import KokoHomeKoko from './kokohome/KokoHomeKoko';

class KokoPage extends Component {
    render() {
        return (
            <div className="section-projects">
                 <h3>  koko homepage</h3> 
                 <KokoHomeHero />
                 {/* <KokoHomeAbout /> */}
                 {/* <KokoHomeKoko /> */}
            </div>
        );
    }
};

export default KokoPage;