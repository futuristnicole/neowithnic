import React, { Component } from 'react';
import Project from '../../../../builders/Project';

class KokoGraphQL extends Component {
    render() {
        return (
            <div>
                <Project  img='../img/Koko.jpg' 
                          alt='GraphQL API' 
                          text='KOKO The Movie GraphQL API' 
                          link='/projects/koko/kokographgl' />
            </div>
        );
    }
};

export default KokoGraphQL;