import React, { Component } from 'react';
import Project from '../../../../builders/Project';

class KokoDB extends Component {
    render() {
        return (
            <div>
                <Project  img='../img/Koko.jpg' 
                          alt='Neo4j database' 
                          text='KOKO The Movie Neo4j database' 
                          link='/projects/koko/kokodb' />
            </div>
        );
    }
};

export default KokoDB;