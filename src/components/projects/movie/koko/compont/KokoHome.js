import React, { Component } from 'react';
import Project from '../../../../builders/Project';


class KokoHome extends Component {
    render() {
        return (
            <div>
                <Project  img='../img/Koko.jpg' 
                          alt='KOKO the Movie Logo' 
                          text='KOKO The Movie Home Page' 
                          link='/projects/koko/home' />
            </div>
        );
    }
};

export default KokoHome;