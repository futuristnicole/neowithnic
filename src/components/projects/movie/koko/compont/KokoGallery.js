import React, { Component } from 'react';
import Project from '../../../../builders/Project';

class KokoGallery extends Component {
    render() {
        return (
            <div>
                <Project  img='../img/Koko.jpg' 
                          alt='Photo Gallery' 
                          text='KOKO The Movie Photo Gallery' 
                          link='/projects/koko/kokogallery' />
               
            </div>
        );
    }
};

export default KokoGallery;