import React, { Component } from 'react';
import KokoHome from './compont/KokoHome';
import KokoGallery from './compont/KokoGallery';
import KokoDB from './compont/kokoDB';
import KokoGraphQL from './compont/KokoGraphQL'

class KOKO extends Component {
    render() {
        return (
            <div className="section-projects">
                <h2 className="heading">KOKO The Movie</h2>
                <p className="pargraph">Koko The Movie website was a project I took on to learn GRANDstack. I designed and built the site.  However, we are not using the site for the more because of hosting cost for the Neo4j database.</p>
                <p className="pargraph">Below are 2 of the pages I built of the site, the GraphQL API and the database design.</p>
                <div className="grid-2">
                    <KokoHome />
                    <KokoGallery /> 
                    <KokoDB />
                    <KokoGraphQL />
                </div>
            </div>
        );
    }
};

export default KOKO;