import React from 'react';

const KokoHomeAbout = () => {
    return (
        <article className="section__about center-text">
        <h2 className="section-headers"><span className="brand">KOKO</span> The Movie</h2>
        <div className="flex__container-home-about">
          <div className="flex__box-home-about">
            <p className="big">An extraordinary story of a young financial wiz, Randy, who suffers a lifetime of heart-aches and heartbreaks, only to discover the purest form of love in his one true companion, a golden retriever name Koko.</p>
          </div>
          <div className="flex__box-home-about">
            <img className="img-contain__about" src="../img/RandyKoko.jpg" alt="Randy and Koko" />
          </div>
        </div>
      </article>
    );
};

export default KokoHomeAbout;