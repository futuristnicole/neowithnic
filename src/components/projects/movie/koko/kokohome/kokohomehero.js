import React from 'react';

const kokohomehero = () => {
    return (
        <div className="background-hero">
            <div className="text-box-center">
                <h1 className="hero-text ">
                    <span className="hero-text__main logoKoko">KOKO</span>
                    <span className="hero-text__sub">How Far You Can Go To Pay Tribute To Your Love?</span>
                </h1>
            </div>
        </div>
    );
};

export default kokohomehero;