import React from 'react';

const KokoHomeKoko = () => {
    return (
        <section className="section__koko">
        <div className="background-koko">
          <div className="center-text__koko">
            <h2 className="">Meet <span className="brand">KOKO</span></h2>
            <p className="big">write something about Koko here !!!!</p>
          </div>
        </div>
      </section>
    );
};

export default KokoHomeKoko;