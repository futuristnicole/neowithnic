import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';


import KOKO from './koko/koko';
import KokoPage from './koko/KokoPage';
import KokoGallerypage from './koko/KokoGallerypage';
import KokoGraphQL from './koko/KokoGraphQLpage';
import KokoNeo4jpage from './koko/KokoNeo4j';

class Projects extends Component {
    render() {
        return (
            <Switch>
                <Route path="/projects/koko/kokodb"  component={KokoNeo4jpage} />
                <Route path="/projects/koko/kokographgl"  component={KokoGraphQL} />
                <Route path="/projects/koko/kokogallery"  component={KokoGallerypage} />
                <Route path="/projects/koko/home" component={KokoPage} />
                <Route path="/projects/koko" component={KOKO} />
            </Switch> 
        );
    }
};

export default Projects;