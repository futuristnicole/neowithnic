import React, { Component } from 'react';
import Project from '../../builders/Project';

class Movie extends Component {
    render() {
        return (
            <div>
                <Project  img='img/Koko.jpg' 
                          alt='KOKO the Movie Logo' 
                          text='Movie Website' 
                          link='/projects/koko' />
            </div>
        );
    }
};

export default Movie;