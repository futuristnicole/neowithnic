import React, { Component } from 'react';
// import { BrowserRouter, Route, Switch } from 'react-router-dom';

import ReadBoot from './ReadBoot'
import Movie from './movie'
import PhoneBook from './PhoneBook'

class ProjectsHome extends Component {
    render() {
        return (
            <section className="section-projects">
                <h1 className="heading">Some of Nicole's Projects include</h1>
                <div className="grid-2">
                    <ReadBoot />
                    <Movie />
                    {/* <PhoneBook />                  */}
                </div>
            </section>
        );
    }
};

export default ProjectsHome;