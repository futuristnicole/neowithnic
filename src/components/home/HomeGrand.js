import React, { Component } from 'react';
import Icon from '../builders/Icon';

class HomeGrand extends Component {
    render() {
        return (
            <section className="section-grandstack">
                <h2 className="h2-section">Nicole codes in...</h2>
                <div className="grid-4">
                    <Icon  img='graphql-logo.png' alt='GraphQL Logo' text='GraphQL' />
                    <Icon  img='react-logo.png' alt='React Logo' text='React' />
                    <Icon  img='apollo-logo.png' alt='Apollo Logo' text='Apollo' />
                    <Icon  img='neo4j_logo.png' alt='Neo4j Logo' text='Neo4j' />
                </div>
            </section>
        );
    }
};

export default HomeGrand;