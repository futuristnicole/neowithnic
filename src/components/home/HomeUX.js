import React, { Component } from 'react';

class HomeUX extends Component {
    render() {
        return (
            <section className="section-uxhome">
                <div className="">
                   <h2><span className="heroText-sub-section">Before becoming a GRANDstack developer Nicole was an</span> 
                   <span className="heroText-main-section">UX Designer and Researcher</span>
                   </h2>
                </div>  
            </section>
        );
    }
};

export default HomeUX;