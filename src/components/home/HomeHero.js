import React, { Component } from 'react';

class HomeHero extends Component {
    render() {
        return (
            <header className="hero-home">
                <div className="text-box-center">
                    <img className="img-contain__homehero" src="../img/GrandStack-Logo.png" alt="Grand Stack Logo" />
                    <h1 className="heroText-home">
                        <span className="heroText-sub-home">Nicole Trapp's </span>
                        <span className="heroText-main-home">GRANDstack</span>
                        <span className="heroText-sub-home"> Portfolio.</span>
                    </h1>
                </div>
               
            </header>
        );
    }
};

export default HomeHero;