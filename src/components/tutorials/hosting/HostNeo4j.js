import React, { Component } from 'react';
import HostNeo4jSandbox from './HostNeo4j/HostNeo4jSandbox';
import HostNeo4jGrapheneBD from './HostNeo4j/HostNeo4jGrapheneBD';
import HostNeo4jGCP from './HostNeo4j/HostNeo4jGCP';

class HostNeo4j extends Component {
    render() {
        return (
            <div className="">
                <p>HostNeo4j  Coming soon</p>
                <h3>Neo4j sandbox</h3>
                    <HostNeo4jSandbox />
                <h3>GrapheneBD</h3>
                    <HostNeo4jGrapheneBD />
                <h3>GCP Compute Engine</h3>
                    <HostNeo4jGCP />
            </div>
        );
    }
};

export default HostNeo4j;