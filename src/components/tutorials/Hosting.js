import React, { Component } from 'react';
import HostNeo4j from './hosting/HostNeo4j';
import HostGraphQL from './hosting/HostGraphQL';
import HostReact from './hosting/HostReact';

class Hosting extends Component {
    render() {
        return (
            <section className="section-tutorials">
                <h1>Hosting Your GRANDstack Site</h1>
                <p>In this tutorial, I am assuming you are making a GRANDstack site as part of a portfolio and not for running a company.   Therefore, I will be looking at low-cost options for hosting that are simple and easy to use.</p>  
                <p>For hosting your GRANDstack, you will need to host three parts </p>
                <ol>
                    <li>Your Neo4j database.</li>
                    <li>Your GraphQL API.</li>
                    <li>Your React website.</li>
                </ol>
                <h2>Hosting your Neo4j Database.</h2>
                    <HostNeo4j />
                <h2>Your GraphQL API.</h2>
                    <HostGraphQL />
                <h2>Your React website.</h2>
                    <HostReact />
            </section>
        );
    }
};

export default Hosting;