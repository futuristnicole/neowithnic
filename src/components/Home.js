import React, { Component } from 'react';
import HomeHero from './home/HomeHero';
import HomeGrand from './home/HomeGrand';
import HomeUX from './home/HomeUX';
;
class Home extends Component {
    render() {
        return (
            <div>
                <HomeHero />
                <HomeGrand />
                <HomeUX />
            </div>
        );
    }
};

export default Home;