import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import SideNav from './menu/SideNav';
import TutorialHome from './tutorials/TutotialHome';
import Hosting from './tutorials/Hosting';
import GraphQL from './tutorials/GraphQL';
import LearnReact from './tutorials/LearnReact';
import Apollo from './tutorials/Apollo';
import Neo4j from './tutorials/Neo4j';
import SCSS from './tutorials/SCSS';

class Tutorials extends Component {
    render() {
        return (
            <div className="sidebar-container">
                <Route path="/tutorials" component={SideNav} />                        
                <main className="sidebar-main"> 
                    <Switch>                     
                        <Route path="/tutorials/hosting"  component={Hosting} />
                        <Route path="/tutorials/graphql"  component={GraphQL} />
                        <Route path="/tutorials/react"  component={LearnReact} /> 
                        <Route path="/tutorials/apollo"  component={Apollo} /> 
                        <Route path="/tutorials/neo4j"  component={Neo4j} /> 
                        <Route path="/tutorials/scss"  component={SCSS} /> 
                        <Route path="/tutorials"  component={TutorialHome} /> 
                    </Switch>           
                </main>    
            </div>
        );
    }
};

export default Tutorials;