import React, { Component } from 'react';

class Icon extends Component {
    render() {
        return (
            <div className="Icon">
                <img className="img-contain__homehero" src={`../img/${this.props.img}`} alt={`${this.props.alt}`} />
               <h4>{this.props.text}</h4> 
            </div>
        );
    }
};

export default Icon;