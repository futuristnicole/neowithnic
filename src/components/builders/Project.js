import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Project extends Component {
    render() {
        return (
            <div className="Project">
                <Link to={`${this.props.link}`} className="link__Project">
                <img className="img-contain__Project" src={`../${this.props.img}`} alt={`${this.props.alt}`} />
               <h4>{this.props.text}</h4> 
               </Link>
            </div>
        );
    }
};

export default Project;