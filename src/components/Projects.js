import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import ProjectsHome from './projects/projectHome/ProjectHome';
import Movie from './projects/movie/movies';


class Projects extends Component {
    render() {
        return (
            <Switch>
                <Route path="/projects/koko"  component={Movie} />
                <Route path="/projects"  component={ProjectsHome} />
            </Switch>  
        );
    }
};

export default Projects;