import React, { Component } from 'react';

import NavItem from './Nav-item'

class SideNav extends Component {
    render() {
        return (
            <div className="sidebar-side">
                <ul className="nav-side">
                    {/* <NavItem link='/tutorials' page='TutorialHome'/> */}
                    <NavItem link='/tutorials/hosting' page='Hosting'/>
                    <NavItem link='/tutorials/graphql' page='GraphQL'/>
                    <NavItem link='/tutorials/react' page='React'/>
                    <NavItem link='/tutorials/apollo' page='Apollo'/>
                    <NavItem link='/tutorials/neo4j' page='Neo4j'/>
                    <NavItem link='/tutorials/scss' page='SCSS'/>
                </ul>
            </div>
        );
    }
};

export default SideNav;