import React, { Component } from 'react';
// import { Link } from 'react-router-dom';

import NavItem from './Nav-item'

class TopNav extends Component {
    render() {
        return (
            <header className="topbar">
                <div className="logo"><span className="logotext-neo">Neo</span>With<span className="logotext-nic">Nic</span></div>
                <nav className="topnav">
                    <ul className="nav-top">
                        <NavItem  link="/" page='Home' />
                        <NavItem  link='/tutorials' page='Tutorials' />
                        <NavItem  link='/projects' page='Projects' />
                        <NavItem  link='/about' page='About' />
                        <NavItem  link='/contact' page='Contact' />
                    </ul>
                </nav>
            </header>
        );
    }
};

export default TopNav;