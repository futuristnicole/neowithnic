import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class SideNavItem extends Component {
    render() {
        return (
            <li className="nav-item">
                <Link className="nav-link" to={`${this.props.link}`}>{this.props.page}</Link>                
            </li>
        );
    }
};

export default SideNavItem;