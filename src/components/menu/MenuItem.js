import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class MenuItem extends Component {
    render() {
        return (
            <Link className="topnav-link" to={`${this.props.link}`}>{this.props.page}</Link>
        );
    }
};

export default MenuItem;