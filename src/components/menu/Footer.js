import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <div className="legal">
                &copy; 2019 by Nicole Trapp. All right reserved. 
            </div>  
        );
    }
};

export default Footer;